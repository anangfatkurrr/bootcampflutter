// Import Class into Main.dart
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';
import 'bangun_datar.dart';

void main(List<String> args) {

  BangunDatar bangundatar = new BangunDatar();
  Lingkaran lingkaran = new Lingkaran(5, 3);
  Persegi persegi = new Persegi(4);
  Segitiga segitiga = new Segitiga(1, 2, 3);

  bangundatar.luas();
  bangundatar.keliling();


  print("Luas Persegi:${persegi.luas()}");
  print("Keliling Persegi:${persegi.keliling()}");

  print("Luas Segitiga:${segitiga.luas()}");
  print("Keliling Lingkaran:${segitiga.keliling()}");

  print("Luas Lingkaran:${lingkaran.luas()}");
  print("Keliling Lingkaran:${lingkaran.keliling()}");
}
