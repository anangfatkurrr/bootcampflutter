import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  double alas;
  double tinggi;
  double b;

  Segitiga(double alas, double tinggi, double b) {
    this.alas = alas;
    this.tinggi = tinggi;
    this.b = b;
  }
  @override
  double luas() {
    return 0.5 * alas * tinggi;
  }

  double keliling() {
    return alas + b + tinggi;
  }
}
