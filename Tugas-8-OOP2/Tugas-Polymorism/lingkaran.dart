import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  double jari;
  double pi;

  Lingkaran(double jari, double pi) {
    this.jari = jari;
    this.pi = pi;
  }
  @override
  double luas() {
    return 3.14 * jari * jari;
  }

  double keliling() {
    return 2 * pi * jari;
  }
}
