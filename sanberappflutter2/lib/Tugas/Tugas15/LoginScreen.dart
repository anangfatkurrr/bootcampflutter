import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.all(25.0),
          child: ListView(
            children: <Widget>[
              Container(
                child: Image.asset(
                  'assets/img/logo.png',
                  fit: BoxFit.contain,
                ),
                height: 230,
                alignment: Alignment.centerRight,
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.only(bottom: 30),
                          child: Text('Start Code Flutter!!',
                              style: TextStyle(
                                  color: Color(0xFF1565C0),
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold)),
                          alignment: Alignment.centerLeft),
                      Container(
                        margin: const EdgeInsets.only(bottom: 30),
                        child: TextField(
                            decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: 'Username',
                        )),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 8),
                        child: TextField(
                            decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: 'Password',
                        )),
                      ),
                      TextButton(
                        onPressed: () {},
                        child: Text("Forgot Password?"),
                      ),
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                            onPressed: () {},
                            child: Text(
                              'Login',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                            style: ElevatedButton.styleFrom(
                                padding: const EdgeInsets.all(10))),
                      ),
                      Container(
                          child: Row(children: <Widget>[
                        Text("You done have an account?",
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.bold)),
                        TextButton(
                            child: Text("Create Account",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold)),
                            onPressed: () {})
                      ]))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
