import 'package:flutter/material.dart';

class Homescreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   // title: Text(""),
      //   actions: <Widget>[
      //     Padding(
      //       padding: const EdgeInsets.all(8.0),
      //       child: Icon(Icons.notifications),
      //       // child: Icon(Icons.shopping_cart),
      //     )
      //   ],
      // ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: ListView(
          children: [
            SizedBox(height: 60),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {})
              ],
            ),
            SizedBox(height: 37),
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: "Welcome,\n",
                    style: TextStyle(color: Colors.blue[200]),
                  ),
                  TextSpan(
                    text: "Anang\n",
                    style: TextStyle(color: Colors.blue[900]),
                  ),
                ],
              ),
              style: TextStyle(fontSize: 30),
            ),
            SizedBox(height: 15),
            Container(
              margin: const EdgeInsets.only(bottom: 30),
              child: TextField(
                  decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 25),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'Search',
              )),
            ),

            SizedBox(height: 10),
            Text(
              "Recomended Place",
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
            ),
            // SizedBox(
            //     height: 250,
            //     child: GridView.count(
            //       padding: EdgeInsets.all(5),
            //       crossAxisCount: 2,
            //       childAspectRatio: 3 / 2,
            //       crossAxisSpacing: 10,
            //       physics: NeverScrollableScrollPhysics(),
            //       children: [
            //         for (var item in places)
            //           Image.asset("assets/img/$item.png")
            //       ],
            //     ))

            SizedBox(
              height: 250,
              child: GridView.count(
                padding: EdgeInsets.all(5),
                crossAxisCount: 2,
                childAspectRatio: 1.491,
                children: [
                  for (var item in places) Image.asset('assets/img/$item.png')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

// final countries = ["Tokyo", "Berlin", "Roma", "Monas"];
final places = ["Tokyo", "Berlin", "Roma", "Monas"];
