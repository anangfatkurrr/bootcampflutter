class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'hilmy',
      message: 'Hello Hilmy',
      time: '12.00',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'riska',
      message: 'hello riska',
      time: '9 march',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'vita',
      message: 'hello vita',
      time: '10 march',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg'),
];
