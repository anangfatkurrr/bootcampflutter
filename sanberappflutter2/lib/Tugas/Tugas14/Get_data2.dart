import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
// import 'Models/user_model.dart';

class GetData2 extends StatefulWidget {
  const GetData2({Key? key}) : super(key: key);

  @override
  _GetData2State createState() => _GetData2State();
}

class _GetData2State extends State<GetData2> {
  final String url = "https://achmadhilmy-sanbercode.my.id/api/v1/news";
  List? data;

  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<String> getJsonData() async {
    var response =
        await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    print(response.body);
    setState(() {
      var convertDataToJson = jsonDecode(response.body);
      data = convertDataToJson['data'];
    });
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Get Data")),
        body: new ListView.builder(
          itemCount: data == null ? 0 : data!.length,
          itemBuilder: (BuildContext context, int index) {
            return new Container(
              margin: const EdgeInsets.all(5.0),
              child: new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => GetData2()),
                          );
                        },
                        child: Container(
                            child: Text(data![index]["title"]),
                            padding: EdgeInsets.all(20.0))),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
