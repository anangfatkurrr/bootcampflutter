// 1. Ternary operator //

// void main(List<String> args) {
//   var jawaban = "t";
//   if (jawaban == "y") {
//     print("anda akan menginstall aplikasi dart");
//   } else {
//     print("aborted");
//   }
// }

// import 'dart:io';

// void main(List<String> args) {
//   print("Apakah Anda ingin Menginstall Aplikasi ini?(y/n):");
//   String inputText = stdin.readLineSync();

//   if (inputText == "y") {
//     print('Anda akan menginstall aplikasi dart');
//   } else {
//     print('aborted');
//   }
// }

// 2. If-else if dan else //

// import 'dart:io';

// void main(List<String> args) {
//   stdout.write('masukan nama: ');
//   var nama = stdin.readLineSync();

//   if (nama == '') {
//     stdout.write('Nama harus diisi\n');
//   } else {
//     stdout.write(
//         'Hallo $nama, Pilih peranmu untuk memulai game (penyihir/guard/wolf): ');
//     var peran = stdin.readLineSync();

//     if (peran == 'penyihir') {
//       stdout.write(
//           'Selamat datang di Dunia Werewolf, $nama\nHalo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!\n');
//     } else if (peran == 'guard') {
//       stdout.write(
//           'Selamat datang di Dunia Werewolf, $nama\nHalo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf.\n');
//     } else if (peran == 'wolf') {
//       stdout.write(
//           'Selamat datang di Dunia Werewolf, $nama\nHalo Werewolf $nama, Kamu akan memakan mangsa setiap malam!\n');
//     } else {
//       stdout.write('Hallo $nama, anda salah memasukan nama peran!\n');
//     }
//   }
// }

// 3. Switch case //

// import 'dart:io';

// void main(List<String> args) {
//   print("Masukan Nama Hari:");
//   String hari = stdin.readLineSync();

//   switch (hari) {
//     case "Senin":
//       {
//         print(
//             "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
//         break;
//       }
//     case "Selasa":
//       {
//         print(
//             "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
//         break;
//       }
//     case "Rabu":
//       {
//         print(
//             "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
//         break;
//       }
//     case "Kamis":
//       {
//         print(
//             "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
//         break;
//       }
//     case "Jumat":
//       {
//         print("Hidup tak selamanya tentang pacar.");
//         break;
//       }
//     case "Sabtu":
//       {
//         print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
//         break;
//       }
//     case "Minggu":
//       {
//         print(
//             "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
//         break;
//       }
//     default:
//       {
//         print("Tidak terjadi apa-apa");
//       }
//   }
// }

// 4. Switch Case //

void main(List<String> args) {
  var hari = 21;
  var bulan = 1;
  var tahun = 1945;

  switch (bulan) {
    case 1:
      print('$hari Januari $tahun');
      break;
    case 2:
      print('$hari Februari $tahun');
      break;
    case 3:
      print('$hari Maret $tahun');
      break;
    case 4:
      print('$hari April $tahun');
      break;
    case 5:
      print('$hari Mei $tahun');
      break;
    case 6:
      print('$hari Juni $tahun');
      break;
    case 7:
      print('$hari Juli $tahun');
      break;
    case 8:
      print('$hari Agustus $tahun');
      break;
    case 9:
      print('$hari September $tahun');
      break;
    case 10:
      print('$hari Oktober $tahun');
      break;
    case 11:
      print('$hari Nopember $tahun');
      break;
    case 12:
      print('$hari Desember $tahun');
      break;
  }
}
