// No. 1 //

// void main(List<String> args) {
//   teriak();
// }

// teriak() {
//   print("Hello Sanbers!");
// }

// No. 2 //

// int kalikan(int num1, int num2) {
//   var result = num1 * num2;
//   return result;
// }

// void main(List<String> args) {
//   var num1 = 12;
//   var num2 = 4;

//   var hasilKali = kalikan(num1, num2);
//   print(hasilKali);
// }

// No. 3 //

// String introduce(String name, int age, String address, String hobby) {
//   return "Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!";
// }

// void main(List<String> args) {
//   var name = "Agus";
//   var age = 30;
//   var address = "Jln. Malioboro, Yogyakarta";
//   var hobby = "Gaming";
//   var perkenalan = introduce(name, age, address, hobby);
//   print(perkenalan);
// }

// No. 4 //

void main(List<String> args) {
  print(findFactorial(6));
}

findFactorial(int no) {
  if (no <= 0) {
    return 1;
  }
  return no * findFactorial(no - 1);
}
