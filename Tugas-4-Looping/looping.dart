// No. 1 Looping While //

// void main(List<String> args) {
//   var i = 2;
//   var loop1 = " - I love coding";
//   var loop2 = " - I will become a mobile developer";

//   print('LOOPING PERTAMA');
//   while (i < 20) {
//     print(i.toString() + loop1);
//     i += 2;
//   }
//   print(i.toString() + loop1);
//   print('LOOPING KEDUA');
//   while (i >= 2) {
//     print(i.toString() + loop2);
//     i -= 2;
//   }
// }

// No. 2 Looping menggunakan for //

// void main(List<String> args) {
//   print("OUTPUT");
//   var santai = " - Santai";
//   var berkualitas = " - Berkualitas";
//   var loveCoding = " - I Love Coding";
//   for (var i = 1; i <= 20; i++) {
//     if (i % 2 != 1) {
//       print(i.toString() + berkualitas);
//     } else if (i % 3 == 0) {
//       print(i.toString() + loveCoding);
//     } else {
//       print(i.toString() + santai);
//     }
//   }
// }

// No. 3 Membuat Persegi Panjang # //

// void main(List<String> args) {
//   int i = 1;
//   var x = 1;
//   var panjang = 8;
//   var lebar = 4;
//   var pagar = '';

//   while (x <= lebar) {
//     while (i <= panjang) {
//       pagar += '#';
//       i++;
//     }
//     print(pagar);
//     pagar = '';
//     i = 1;
//     x++;
//   }
// }

// No. 4 Membuat Tangga //

void main(List<String> args) {
  int i = 1;
  int x = 1;
  // var tinggi = 7;
  var alas = 7;
  var pagar = "";

  for (i = 1; i <= alas; i++) {
    for (x = 1; x <= i; x++) {
      pagar += "#";
    }
    print(pagar);
    pagar = "";
  }
}
